import torch

from lbad.utils import to_torch_and_device
from lbad.utils.sampling import uniform_ball
from lbad.data.structures import BallSamplerMixin, MidpointStructure


class ToyDataset(BallSamplerMixin):
    """
    Class for toy datasets which have an underlying known manifold.
    """

    def __init__(
        self,
        manifold: MidpointStructure,
        transform=None,
        sample_tuples=True,
        sampling_strategy="rejection",
        device="cpu",
    ):
        """Initialization.

        Parameters
        ----------
        manifold: manifold
            Underlying manifold.
        transform: a function transforming a point on the manifold
            Function transforming a point on the manifold, e.g. rendering it as an image
             or converting to a different
            format, if None: identity function
        sample_tuples: boolean, default:True
            Whether to sample tuples or just points.
        sampling_strategy: string
            Sampling strategy, one of rejection, rejection_conditional, tangential
        device: string
            device on which the returned tensor should be located
        """
        super().__init__(sample_tuples=sample_tuples, device=device)
        self.manifold = manifold
        self.transform = transform if transform else lambda x: x
        self.has_transform = transform is not None
        self.sampling_strategy = sampling_strategy

    def sample(
        self, num_samples, eps=None, eps_rel=None, eps_min=None, eps_min_rel=None
    ):
        """Sample either a tuple of (x, y, distances, interpolations) or just a point x.

        Parameters
        ----------
        num_samples: int
            number of samples to generate
        eps: float
            maximal distance of partner (absolute)
        eps_rel: float between 0 and 1
            maximal distance of partner relative to diameter
        eps_min: float
            minimal distance of partner (absolute)
        eps_min_rel: float between 0 and 1
            minimal distance of partner relative to diameter

        Returns
        -------
        If self.sample_tuples = True:
        samples: dictionary of tuples consisting of
            x: array of shape [num_samples, ...], points on manifold
            y: array of shape [num_samples, ...], points on manifold
            distances: array of shape [num_samples, ], squared distances
            interpolations: array of shape [num_samples, ...], geodesic midpoints
            between x and y
        If self.sample_tuples = False:
        x: array of shape [num_samples, ...]
            samples of points
        """
        if not self.sample_tuples:
            return self.transform(super().sample(num_samples))
        super().check_eps(eps, eps_rel, eps_min, eps_min_rel)
        if eps is None:
            eps = eps_rel * self.manifold.diameter
        if eps_min is None:
            eps_min = eps_min_rel * self.manifold.diameter
        if self.sampling_strategy == "rejection":
            x = []
            y = []
            interpolations = []
            squared_dist = []
            num = 0
            while num < num_samples:
                x_temp = self.manifold.random_point(2 * num_samples, device=self.device)
                y_temp = self.manifold.random_point(2 * num_samples, device=self.device)
                dist_temp = to_torch_and_device(
                    self.manifold.squared_dist(x_temp, y_temp), self.device
                )
                valid_indices = torch.logical_and(
                    dist_temp < eps**2, dist_temp > eps_min**2
                )
                num_valid_new = min(sum(valid_indices), num_samples - num)
                if num_valid_new > 0:
                    x_temp = x_temp[valid_indices][:num_valid_new]
                    y_temp = y_temp[valid_indices][:num_valid_new]
                    dist_temp = dist_temp[valid_indices][:num_valid_new]
                    x.append(x_temp)
                    y.append(y_temp)
                    midpoint = self.manifold.midpoint(x_temp, y_temp)
                    interpolations.append(to_torch_and_device(midpoint, self.device))
                    squared_dist.append(dist_temp)
                    num = num + num_valid_new
            x = torch.cat(x, dim=0)
            y = torch.cat(y, dim=0)
            interpolations = torch.cat(interpolations, dim=0)
            squared_dist = torch.cat(squared_dist, dim=0)
            samples = {
                "x": x,
                "y": y,
                "interpolations": interpolations,
                "distances": squared_dist,
            }
        else:
            samples = super().sample(num_samples, eps=eps, eps_min=eps_min)
        if self.has_transform:
            for name in ["x", "y", "interpolations"]:
                samples[name + "_p"] = samples[name]
                samples[name] = self.transform(samples[name])
        return samples

    def sample_point(self, num_samples: int, transform=False):
        """Sample a point on the manifold.

        Parameters
        ----------
        num_samples: int
            number of samples to generate
        transform: boolean, default:false
            whether to apply transform to generated points


        Returns
        -------
        x: array of shape [num_samples, ...]
            point on the manifold, transformed if transform=True

        """
        x = self.manifold.random_point(num_samples, device=self.device)
        return self.transform(x) if transform else x

    def sample_partner(
        self,
        x: torch.Tensor,
        eps: float = None,
        eps_rel: float = None,
        eps_min: float = None,
        eps_min_rel: float = None,
    ):
        """Sample a partner within distance eps to each parameter in x .

        Parameters
        ----------
        x: array of shape [batch_dim, ...]
            points to find to find a partner
        eps: float
            maximal distance of partner (absolute)
        eps_rel: float between 0 and 1
            maximal distance of partner relative to diameter
        eps_min: float
            minimal distance of partner (absolute)
        eps_min_rel: float between 0 and 1
            minimal distance of partner relative to diameter

        Returns
        -------
        y: array of shape [batch_dim, ...], same shape as x
            partners to each x
        dist: array of shape [batch_dim,]
            squared distances between x and y
        interpolation: array of shape [batch_dim, ...], same shape as x
            geodesic midpoint between x and y
        """
        super().check_eps(eps, eps_rel, eps_min, eps_min_rel)
        if eps is None:
            eps = self.manifold.diameter * eps_rel
        if eps_min is None:
            eps_min = self.manifold.diameter * eps_min_rel
        if (
            self.sampling_strategy == "rejection_conditional"
            or self.sampling_strategy == "rejection"
        ):
            y = torch.empty_like(x)
            interpolations = torch.empty_like(x)
            squared_dist = -torch.ones(x.shape[0])
            while sum(squared_dist < 0) > 0:
                y_temp = self.manifold.random_point(x.shape[0], device=x.device)
                dist_temp = self.manifold.squared_dist(x, y_temp)
                valid = torch.logical_and(
                    dist_temp >= eps_min**2, dist_temp <= eps**2
                )
                y[valid] = y_temp[valid]
                interpolations[valid] = self.manifold.midpoint(x[valid], y_temp[valid])
                squared_dist[valid] = dist_temp[valid]
        elif (
            self.sampling_strategy == "tangential"
            or self.sampling_strategy == "tangential_cone"
        ):
            if self.sampling_strategy == "tangential":
                u_euclidean = uniform_ball(
                    num_samples=len(x),
                    dim=self.manifold.dim,
                    radius=eps,
                    radius_min=eps_min,
                    device=self.device,
                )
            else:
                u_euclidean = self.manifold.uniform_cone(
                    num_samples=len(x),
                    radius=eps,
                    radius_min=eps_min,
                    device=self.device,
                )
            tangent_vec = self.manifold.tangent_isometry(u_euclidean, x)
            y = self.manifold.exp(tangent_vec, x)
            interpolations = self.manifold.exp(0.5 * tangent_vec, x)
            squared_dist = torch.sum(u_euclidean**2, axis=-1)
        else:
            raise ValueError(
                f"Unknown sampling strategy {self.sampling_strategy}. Please specify "
                f'one of "tangential", "tangential_cone" and "rejection_conditional".'
            )
        return y, squared_dist, interpolations


class InfiniteDataLoader:
    """Dataloader which can possibly generate an infinite amount of data samples.

    A dataloader which generates samples from the underlying toy dataset.
    Finish after dataset_size generated samples, using batches of specified batch_size.
    Last batch maybe smaller.
    Sampling params can optionally be specified as a dict to be used as additional
    parameters passed to the dataset's sample function.
    """

    def __init__(
        self,
        dataset: ToyDataset,
        batch_size: int,
        dataset_size: int = None,
        sampling_params: dict = None,
    ):
        """Initialize.

        Parameters
        ----------
        dataset: ToyDataset
            underlying toy dataset
        batch_size: int
            batch size
        dataset_size: int, default: batch_size
            total number of samples to generated before terminating
        sampling_params: dict, default: None
            dictionary of sampling parameters passed to the dataset's sample function
        """
        if dataset_size is None:
            dataset_size = batch_size
        self.num_total_samples = dataset_size
        self.dataset = dataset
        self.batch_size = batch_size
        self.sampling_params = sampling_params
        self.num = self.num_total_samples

    def __iter__(self):
        self.num_samples = 0
        return self

    def __next__(self):
        if self.num_samples < self.num_total_samples:
            remaining_samples = self.num_total_samples - self.num_samples
            batch_size = min(self.batch_size, remaining_samples)
            if self.sampling_params is not None:
                sample = self.dataset.sample(batch_size, **self.sampling_params)
            else:
                sample = self.dataset.sample(batch_size)
            self.num_samples += batch_size
            return sample
        else:
            raise StopIteration
