import torch
from pytorch3d.io import load_objs_as_meshes
from pytorch3d.renderer import (
    look_at_view_transform,
    FoVPerspectiveCameras,
    PointLights,
    RasterizationSettings,
    MeshRenderer,
    MeshRasterizer,
    SoftPhongShader,
    TexturesVertex,
)
from pytorch3d.structures import Meshes

from lbad.utils import to_torch_and_device


class Renderer3d:
    def __init__(
        self,
        im_width,
        device,
        obj_filepath,
        color=(177, 200, 0),
        dist=2.7,
        elev=90,
        azim=0,
    ):
        self.im_width = im_width
        self._device = device
        rot, t = look_at_view_transform(dist, elev, azim)
        self.t = t
        self.obj_filepath = obj_filepath
        self.color = color
        self.dist = dist
        cameras = FoVPerspectiveCameras(device=device, R=rot, T=t)
        raster_settings = RasterizationSettings(
            image_size=im_width,
            blur_radius=0.0,
            faces_per_pixel=1,
        )
        # vertices, faces_idx, aux = load_obj(obj_filepath)
        mesh = load_objs_as_meshes([obj_filepath], device=self.device)
        self.faces = mesh.faces_list()[0].to(device)
        self.vertices = mesh.verts_list()[0].to(device)
        self.textures = mesh.textures

        lights = PointLights(device=device, location=[[0.0, 3, 0.0]])
        self.renderer = MeshRenderer(
            rasterizer=MeshRasterizer(cameras=cameras, raster_settings=raster_settings),
            shader=SoftPhongShader(device=device, cameras=cameras, lights=lights),
        )

        if self.textures is None:
            # Initialize each vertex to be of specified color.
            vertices_rgb = (
                torch.tensor(color)
                .reshape(1, 3)
                .expand(self.vertices.shape)
                .unsqueeze(0)
                / 255
            )  # (1, V, 3)
            self.textures = TexturesVertex(verts_features=vertices_rgb.to(device))

    def calculate_mesh(self, rotations: torch.Tensor) -> Meshes:
        """Calculate rotated meshes.

        Parameters
        ----------
        rotations: tensor of shape (N, 3, 3)
            rotations
        Returns
        -------
        meshes: Meshes object containing N meshes
        """
        # vertices of shape (N, num_vertices, 3)
        vertices = torch.matmul(
            rotations.unsqueeze(1), self.vertices.reshape(1, -1, 3, 1)
        ).squeeze(-1)
        N = rotations.shape[0]
        meshes = Meshes(list(vertices), [self.faces] * N, self.textures.extend(N))
        return meshes

    def render(self, rot, plotting_format=False) -> torch.Tensor:
        """Render the object using renderer with parameters including camera position
        specified in initialization of class by rotating vertices using given rotations.

        Parameters
        ----------
        rot: tensor or numpy array of shape (N, 3, 3)
            rotations
        plotting_format: bool
            determine shape of returned image

        Returns
        -------
        images: tensor of shape (N, im_width, im_width, 3) if plotting_format is True,
            else shape (N, 3, im_width, im_width)
            rendered images
        """
        rot = to_torch_and_device(rot, device=self.device).reshape(-1, 3, 3)
        meshes = self.calculate_mesh(rot)
        images = self.renderer(meshes)
        if not plotting_format:
            images = torch.movedim(images, -1, 1)[:, :3]
        else:
            images = images[..., :3].cpu()
        return images

    @property
    def device(self):
        return self._device

    @device.setter
    def device(self, value):
        self.renderer.to(value)
        self.textures.to(value)
        self.vertices = self.vertices.to(value)
        self.faces = self.faces.to(value)
