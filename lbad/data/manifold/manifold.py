from abc import ABC, abstractmethod

import torch

from lbad.data.structures import MidpointStructure
from lbad.utils import to_torch_and_device


class MidpointManifoldWithNormalCoordinates(MidpointStructure, ABC):
    """
    Abstract base class which has additional methods needed when tangent space sampling
    should be supported.
    """

    @abstractmethod
    def exp(self, tangent_vec, base_point, **kwargs):
        """Compute exp map of a base point in tangent vector direction.

        Parameters
        ----------
        tangent_vec: array-like of shape {[num_samples, d], [1, d]}
            tangent vector at base point
        base_point: array-like of shape {[num_samples, d], [1, d]}
            base point

        Returns
        -------
        exp: array-like of shape [num_samples, d]
            riemannian exponential
        """
        pass

    @abstractmethod
    def log(self, point, base_point, **kwargs):
        """Compute log map between point and base point.

        Parameters
        ----------
        point: array-like of shape {[num_samples, d], [1, d]}
            target point
        base_point: array-like of shape {[num_samples, d], [1, d]}
            base point

        Returns
        -------
        log: array-like of shape [num_samples, d]
            Riemannian logarithm
        """

    def midpoint(self, x, y, **kwargs):
        """Calculate geodesic midpoint between x and y.

        Parameters
        ----------
        x: array-like of shape [num_samples, ...]
            Points on manifold.
        y: array-like of shape [num_samples, ...]
            Points on manifold.

        Returns
        -------
        midpoints: array-like of shape [num_samples, ...]
            Geodesic midpoints between x and y.
        """
        return self.geodesic_interpolation(x, y, 0.5)

    def geodesic_interpolation(self, x, y, alpha):
        """Calculate weighted geodesic interpolation between x and y with weight alpha.

        Parameters
        ----------
        x: array-like of shape [num_samples, ...]
            Points on manifold.
        y: array-like of shape [num_samples, ...]
            Points on manifold.
        alpha: float
            Interpolation weight.

        Returns
        -------
        interpolation: array-like of shape [num_samples, ...]
            Geodesic interpolation between x and y.

        Notes
        -----
        Uses logarithm and exponential.
        """
        return self.exp(alpha * self.log(y, x), x)

    @abstractmethod
    def tangent_isometry(self, euclidean_vec, base_point, **kwargs):
        r"""Isometric map between euclidean space and tangent space

        Mathematically:  :math:`\iota: \R^dim \leftarrow T_p M`, where dim is the
        dimension of the manifold, and p is the base point,
        :math:`g(\iota(v), \iota(w)) = \langle v,w\rangle`.

        Parameters
        ----------
        euclidean_vec: array-like of shape {[num_samples, dim], [1, dim]}
            vector in euclidean space, either one for each base point or broadcast
        base_point: array-like of shape {[num_samples, d], [1, d]}
            base point, either one for each vector or broadcast

        Returns
        -------
        tangent_vec: array-like of shape [num_samples, d]
            tangent vector at base point

        """
        pass

    @abstractmethod
    def uniform_cone(self, num_samples: int, radius=None, radius_min=0.0, device="cpu"):
        """Sample a cone in dim-dimensional space.

        Parameters
        ----------
        num_samples: int
            number of samples
        radius: float
            maximal radius
        radius_min: float
            minimal radius
        device: string
            device

        Returns
        -------
        samples: torch.Tensor of shape [num_samples, self.dim]
            samples in cone
        """
        pass


class GeomstatsMidpointManifold(MidpointStructure):
    """
    MidpointManifold based on a geomstats manifold using exp and log for midpoint
    computation.
    """

    def __init__(self, manifold, diameter: float = None):
        """Initialization.

        Parameters
        ----------
        manifold: Manifold
            Geomstats manifold.
        diameter: float
            Diameter of manifold. If None, compute using 1000 samples of manifold.
        """
        self.manifold = manifold
        self.dim = self.manifold.dim
        self.diameter = diameter
        if self.diameter is None:
            self.diameter = self.compute_diameter()

    def compute_diameter(self, num_samples=1000) -> float:
        """Compute the diameter of the manifold.

        Parameters
        ----------
        num_samples: int
            Number of samples to use for computation of diameter.

        Returns
        -------
        diameter: float
            Computed diameter if self.diameter is None, else self.diameter

        """
        if self.diameter is None:
            points = self.random_point(num_samples)
            return self.manifold.metric.diameter(points)
        else:
            return self.diameter

    def random_point(self, num_samples: int, device="cpu") -> torch.Tensor:
        """Generate random points on the manifold.

        Parameters
        ----------
        num_samples: int
            Number of samples to generate.
        device: string
            device on which the returned tensor should be located

        Returns
        -------
        x: array-like of shape [num_samples, ...]
            Generated random points on manifold.
        """
        points = self.manifold.random_point(num_samples)
        return to_torch_and_device(points, device=device)

    def geodesic_interpolation(self, x, y, alpha, **kwargs):
        """Calculate weighted geodesic interpolation between x and y with weight alpha.

        Parameters
        ----------
        x: array-like of shape [num_samples, ...]
            Points on manifold.
        y: array-like of shape [num_samples, ...]
            Points on manifold.
        alpha: float
            Interpolation weight.

        Returns
        -------
        interpolation: array-like of shape [num_samples, ...]
            Geodesic interpolation between x and y.

        Notes
        -----
        Uses logarithm and exponential supplied by geomstats.
        """
        logarithm = self.manifold.metric.log(y, base_point=x, **kwargs)
        return self.manifold.metric.exp(alpha * logarithm, base_point=x, **kwargs)

    def midpoint(self, x, y, **kwargs):
        """Calculate geodesic midpoint between x and y.

        Parameters
        ----------
        x: array-like of shape [num_samples, ...]
            Points on manifold.
        y: array-like of shape [num_samples, ...]
            Points on manifold.

        Returns
        -------
        midpoints: array-like of shape [num_samples, ...]
            Geodesic midpoints between x and y.

        Notes
        -----
        Uses logarithm and exponential supplied by geomstats.
        """
        return self.geodesic_interpolation(x, y, 0.5)

    def squared_dist(self, x, y, **kwargs):
        """Calculate squared distance between x and y.

        Parameters
        ----------
        x: array-like of shape [num_samples, ...]
            Points on manifold.
        y: array-like of shape [num_samples, ...]
            Points on manifold.
        Returns
        -------
        squared_dist: array-like of shape [num_samples,]
            Squared distances between points.
        """
        return self.manifold.metric.squared_dist(x, y, **kwargs)

    def exp(self, tangent_vec, base_point, **kwargs):
        """Compute exp map of a base point in tangent vector direction.

        Parameters
        ----------
        tangent_vec: array-like of shape {[num_samples, d], [1, d]}
            tangent vector at base point
        base_point: array-like of shape {[num_samples, d], [1, d]}
            base point

        Returns
        -------
        exp: array-like of shape [num_samples, d]
            riemannian exponential
        """
        return self.manifold.metric.exp(tangent_vec, base_point, **kwargs)

    def log(self, point, base_point, **kwargs):
        """Compute log map between point and base point.

        Parameters
        ----------
        point: array-like of shape {[num_samples, d], [1, d]}
            target point
        base_point: array-like of shape {[num_samples, d], [1, d]}
            base point

        Returns
        -------
        log: array-like of shape [num_samples, d]
            Riemannian logarithm
        """
        return self.manifold.metric.log(point, base_point, **kwargs)
