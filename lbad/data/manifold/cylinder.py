from lbad.data.manifold import MidpointManifoldWithNormalCoordinates
import torch
import numpy as np

from lbad.data.manifold.rendering_utils import (
    render_ellipse_cov,
    astroid,
    render_via_implicit,
    ellipse,
)
from lbad.data.toystructures import ToyDataset
from lbad.utils import to_torch_and_device
from lbad.utils.sampling import rand_tensor, uniform_ball


class Cylinder(MidpointManifoldWithNormalCoordinates):
    r"""Cylinder  :math:`S^1 \times [-r_1, r_1] \times \dots \times [-r_k, r_k]`.

    Points on the cylinder are represented extrinsically in k+2 dimensions using polar
    coordinates.
    """

    def __init__(self, interval_radii):
        super().__init__()
        interval_radii = np.asarray(interval_radii)
        self.dim = 1 + len(interval_radii)
        self.interval_radii = interval_radii
        self.diameter = (np.pi**2 + np.sum((2 * interval_radii) ** 2)) ** 0.5
        self.embed_dim = self.dim + 1
        self.r0 = min(np.pi, min(self.interval_radii))

    def log(self, point: torch.Tensor, base_point: torch.Tensor, **kwargs):
        xTy = torch.clamp(
            torch.sum(base_point[:, :2] * point[:, :2], dim=-1, keepdim=True), -1, 1
        )
        eps = np.finfo(np.float64).eps
        factor = (torch.arccos(xTy) + eps) / ((1 - xTy**2) ** 0.5 + eps)
        res_s1 = (point[:, :2] - base_point[:, :2] * xTy) * factor
        res_rect = point[:, 2:] - base_point[:, 2:]
        return torch.cat([res_s1, res_rect], dim=-1)

    def exp(
        self, tangent_vec: torch.Tensor, base_point: torch.Tensor, **kwargs
    ) -> torch.Tensor:
        """Compute exp map of a base point in tangent vector direction.

        Parameters
        ----------
        tangent_vec: array-like of shape {[num_samples, embed_dim], [1, embed_dim]}
            tangent vector at base point
        base_point: array-like of shape {[num_samples, embed_dim], [1, embed_dim]}
            base point

        Returns
        -------
        exp: array-like of shape [num_samples, embed_dim]
            riemannian exponential
        """
        tangent_vec_s1 = tangent_vec[:, :2]
        norm_v = torch.sum(tangent_vec_s1**2, dim=1, keepdim=True) ** 0.5
        point_s1 = base_point[:, :2] * torch.cos(norm_v) + tangent_vec_s1 * torch.sinc(
            norm_v / np.pi
        )
        point_rect = base_point[:, 2:] + tangent_vec[:, 2:]
        point = torch.cat([point_s1, point_rect], dim=1)
        return torch.where(
            self.belongs(point).unsqueeze(1), point, torch.ones_like(point) * np.nan
        )

    def belongs(self, x: torch.Tensor) -> torch.Tensor:
        """Check whether x belongs to manifold.

        Parameters
        ----------
        x: torch.Tensor of shape [num_samples, embed_dim]
            points in ambient space

        Returns
        -------
        belong: torch.Tensor of shape [num_samples,]
            boolean tensor, true if point is on manifold
        """
        is_s1 = torch.isclose(
            torch.sum(x[:, :2] ** 2, dim=1) ** 0.5, torch.tensor(1.0), rtol=1e-3
        )
        is_rect = torch.all(
            torch.stack(
                [
                    torch.abs(x[:, i + 2]) <= radius
                    for i, radius in enumerate(self.interval_radii)
                ]
            ),
            dim=0,
        )
        return torch.logical_and(is_s1, is_rect)

    def tangent_space(self, base_point: torch.Tensor) -> torch.Tensor:
        """Compute ONB of tangent space at point x.

        Parameters
        ----------
        base_point: torch.Tensor of shape [num_samples, embed_dim]
            point on the manifold

        Returns
        -------
        basis: tensor of shape [num_samples, dim, embed_dim]
            basis of tangent space at x
        """
        # dim unit vectors, shape (num_samples, dim, embed_dim)
        tangent_basis = torch.tile(
            torch.eye(self.embed_dim)[1 : self.dim + 1][None, ...],
            (len(base_point), 1, 1),
        )
        tangent_basis = tangent_basis.to(base_point.device)
        # change the first one
        tangent_basis[:, 0, 0] = -base_point[:, 1]
        tangent_basis[:, 0, 1] = base_point[:, 0]
        return tangent_basis

    def tangent_isometry(
        self, euclidean_vec: torch.Tensor, base_point: torch.Tensor, **kwargs
    ):
        """Isometric map between euclidean space and tangent space, embedded.

        Parameters
        ----------
        euclidean_vec: torch.Tensor of shape {[num_samples, dim], [1, dim]}
            vector in euclidean space, either one for each base point or broadcast
        base_point: torch.Tensor of shape {[num_samples, embed_dim], [1, embed_dim]}
            base point, either one for each vector or broadcast

        Returns
        -------
        tangent_vec: array-like of shape [num_samples, embed_dim]
            tangent vector at base point

        """
        tangent_basis = self.tangent_space(base_point)  # [num_samples, dim, embed_dim]
        euclidean_vec = torch.unsqueeze(euclidean_vec, -1)  # [{num_samples, 1}, dim, 1]
        direction = -torch.sign(base_point[:, 2:]).unsqueeze(1)
        direction = torch.where(direction == 0, torch.ones_like(direction), direction)
        tangent_basis[:, :, 2:] = tangent_basis[:, :, 2:] * direction
        tangent_vec = tangent_basis * euclidean_vec  # [num_samples, dim, embed_dim]
        return torch.sum(tangent_vec, dim=1)

    def uniform_cone(
        self, num_samples: int, radius=None, radius_min=0.0, device="cpu"
    ) -> torch.Tensor:
        """Sample a cone in dim-dimensional space.

        In this case, the "cone" is given as a ball intersected with the upper quadrant
        {x_i>=0 for all i}.

        Parameters
        ----------
        num_samples: int
            number of samples
        radius: float
            maximal radius
        radius_min: float
            minimal radius
        device: string
            device

        Returns
        -------
        samples: torch.Tensor of shape [num_samples, self.dim]
            samples in cone
        """
        if radius > self.r0:
            raise ValueError(
                f"Radius r={radius:.2f} larger than allowed r0={self.r0:.2f}."
            )
        samples = uniform_ball(
            num_samples, self.dim, radius=radius, radius_min=radius_min, device=device
        )
        samples[:, 1:] = torch.abs(samples[:, 1:])
        return samples

    def squared_dist(self, x: torch.Tensor, y: torch.Tensor, **kwargs) -> torch.Tensor:
        d1 = torch.acos(torch.sum(x[:, :2] * y[:, :2], dim=1)) ** 2
        d2 = torch.sum((x[:, 2:] - y[:, 2:]) ** 2, dim=1)
        return d1 + d2

    def random_point(self, num_samples: int, device="cpu") -> torch.Tensor:
        high = torch.tensor(self.interval_radii, device=device).reshape(1, -1)
        x = rand_tensor((num_samples, self.dim - 1), -high, high, device=device)
        angles = rand_tensor((num_samples, 1), 0, 2 * np.pi, device=device)
        return self.intrinsic_to_extrinsic(angles, x).to(torch.float32)

    @staticmethod
    def intrinsic_to_extrinsic(angles, x):
        """Convert angles to polar coordinates, leave x.

        Parameters
        ----------
        angles: torch.Tensor of shape (n, 1)
        x: torch.Tensor of shape (n, self.dim-1)

        Returns
        -------
        x: torch.Tensor of shape (n, self.embed_dim)
        """

        x0 = torch.cos(angles)
        x1 = torch.sin(angles)
        x = torch.cat([x0, x1, x], dim=-1)
        return x

    def extrinsic_to_intrinsic(self, x):
        """Convert extrinsic to intrinsic, i.e. polar, representation.

        Parameters
        ----------
        x: torch.Tensor of shape (n, self.embed_dim)
            points on cylinder

        Returns
        -------
        y: torch.Tensor of shape (n, self.dim-1)
            (angle, pos) representation
        """
        if not torch.all(self.belongs(x)):
            raise ValueError("Not all points lie on the Cylinder.")
        angles = torch.atan2(x[:, [1]], x[:, [0]])
        pos = x[:, 2:]
        return torch.cat([angles, pos], dim=1)

    def geodesic_interpolation(self, x, y, alpha):
        x_intr = self.extrinsic_to_intrinsic(x)
        y_intr = self.extrinsic_to_intrinsic(y)
        angle_x = x_intr[:, [0]]
        angle_y = y_intr[:, [0]]
        pos_x = x_intr[:, 1:]
        pos_y = y_intr[:, 1:]
        angle_xy = average_periodic(angle_x, angle_y, alpha, period=2 * np.pi)
        pos_xy = (1 - alpha) * pos_x + alpha * pos_y
        return self.intrinsic_to_extrinsic(angle_xy, pos_xy)

    def data_range(self):
        """Return per feature minimum and maximum.

        Returns
        -------
        data_min, data_max: np.array of shape [self.embed_dim]
            per feature minimum and maximum
        """
        data_min = np.asarray([-1, -1] + [-radius for radius in self.interval_radii])
        data_max = -data_min
        return data_min, data_max


def average_periodic(p1, p2, alpha, period=2 * np.pi):
    """Interpolate values using a given period, i.e. on R/period*R

    Parameters
    ----------
    p1: tensor of shape (n, 1)
        first set of points
    p2: tensor of shape (n, 1)
        second set of points
    alpha: float
        weighting coefficient
    period: float, optional, default: 2*pi
        periodicity of p1 and p2 (e.g. 2pi)

    Returns
    -------
    average: tensor of shape (n, 1)
        averages
    """
    p1 = torch.remainder(p1, period)
    p2 = torch.remainder(p2, period)
    angle2_options = torch.cat([p2 - period, p2, p2 + period], dim=1)  # shape (n, 3)
    diffs = torch.abs(angle2_options - p1)
    angle2_choice = torch.unsqueeze(torch.argmin(diffs, dim=1), dim=1)  # shape (n, )
    average = (1 - alpha) * p1 + alpha * torch.take_along_dim(
        angle2_options, angle2_choice, dim=1
    )
    return torch.remainder(average, period)


class CylinderEllipseRendererCov:
    def __init__(self, im_width, device, k=0, im_range=(-1, 1), aspect=3, scale=1):
        self.aspect = aspect
        self.scale = scale
        self.device_ = device
        self.im_width = im_width
        self.k = k
        self.im_range = im_range
        x = np.linspace(im_range[0], im_range[1], im_width)
        xx, yy = np.meshgrid(x, x, indexing="xy")
        self.pos = torch.from_numpy(np.stack((xx, yy), axis=2)).to(
            device=device, dtype=torch.float32
        )

    @property
    def device(self):
        return self.device_

    @device.setter
    def device(self, d):
        self.device_ = d
        self.pos.to(d)

    def render(self, cylindrical_coords: torch.Tensor, plotting_format=False):
        """Render cylindrical coordinates as ellipses, with 2pi = 180° rotation

        Parameters
        ----------
        cylindrical_coords: torch.Tensor of shape [num_samples, 4]
            first two coordinates are polar coordinates of circle with radius 1, last
            two coordinates are position
        plotting_format : bool (default: True)
            specify output format


        Returns
        -------
        images: torch.Tensor of shape [num_samples, 1, im_width, im_width]
            if plotting_format=False,
            else torch.Tensor of shape [num_samples, im_width, im_width]
            rendered images

        """
        # cylinder coordinates: cos(2x), sin(2x), position_x, position_y
        cos_sq = (cylindrical_coords[:, 0] + 1) / 2
        sin_sq = (1 - cylindrical_coords[:, 0]) / 2
        sin_cos = cylindrical_coords[:, 1] / 2
        cov_mat = torch.stack(
            [
                torch.stack(
                    [1 + (self.aspect - 1) * cos_sq, sin_cos * (self.aspect - 1)]
                ),
                torch.stack(
                    [sin_cos * (self.aspect - 1), 1 + (self.aspect - 1) * sin_sq]
                ),
            ]
        )
        cov_mat = self.scale**2 * torch.movedim(cov_mat, -1, 0)
        return render_ellipse_cov(
            self.pos,
            cov_mat,
            cylindrical_coords[:, [2]],
            cylindrical_coords[:, [3]],
            plotting_format,
            k=self.k,
        )


class CylinderImplicitRenderer:
    def __init__(
        self,
        im_width,
        device,
        k=0,
        cylinder_extent=1,
        render_func=astroid,
        symmetry=4,
        scale=1.0,
        **render_args,
    ):
        self.symmetry = symmetry
        self.device_ = device
        self.im_width_ = im_width
        self.k = k
        self.render_func = render_func
        self.cylinder_extent = cylinder_extent
        self.pos = self.setup_pos(im_width, device)
        self.render_args = render_args
        self.scale = scale

    @staticmethod
    def setup_pos(im_width, device):
        x = np.linspace(-1, 1, im_width)
        xx, yy = np.meshgrid(x, x, indexing="xy")
        return torch.from_numpy(np.stack((xx, yy), axis=2)).to(
            device=device, dtype=torch.float32
        )

    @property
    def device(self):
        return self.device_

    @device.setter
    def device(self, d):
        self.device_ = d
        self.pos.to(d)

    @property
    def im_width(self):
        return self.im_width_

    @im_width.setter
    def im_width(self, im_width_new):
        self.im_width_ = im_width_new
        self.pos = self.setup_pos(im_width=self.im_width, device=self.device)

    def render(self, cylindrical_coords: torch.Tensor, plotting_format=False):
        """Render cylindrical coordinates using given function with given periodicity.

        Parameters
        ----------
        cylindrical_coords: torch.Tensor of shape [num_samples, 4]
            first two coordinates are polar coordinates of circle with radius 1, last
            two coordinates are position
        plotting_format : bool (default: True)
            specify output format


        Returns
        -------
        images: torch.Tensor of shape [num_samples, 1, im_width, im_width]
            if plotting_format=False,
            else torch.Tensor of shape [num_samples, im_width, im_width]
            rendered images

        """
        # cylinder coordinates: cos(symmetry*x), sin(symmetry*x), position_x, position_y
        angle = torch.atan2(cylindrical_coords[:, 1], cylindrical_coords[:, 0])
        angle_normalized = angle / self.symmetry
        return render_via_implicit(
            self.render_func,
            self.pos,
            angle_normalized,
            cylindrical_coords[:, [2]] / self.cylinder_extent,
            cylindrical_coords[:, [3]] / self.cylinder_extent,
            plotting_format=plotting_format,
            k=self.k,
            scale=self.scale,
            **self.render_args,
        )


class CylinderAstroidRenderer(CylinderImplicitRenderer):
    def __init__(
        self,
        im_width,
        device,
        k=0,
        cylinder_extent=1,
        scale=1.0,
    ):
        super().__init__(
            im_width=im_width,
            device=device,
            k=k,
            cylinder_extent=cylinder_extent,
            render_func=astroid,
            symmetry=4,
            scale=scale,
        )


class CylinderEllipseRenderer(CylinderImplicitRenderer):
    def __init__(self, im_width, device, k=0, cylinder_extent=1, scale=1.0, aspect=2.0):
        super().__init__(
            im_width=im_width,
            device=device,
            k=k,
            cylinder_extent=cylinder_extent,
            render_func=ellipse,
            symmetry=2,
            scale=scale,
            aspect=aspect,
        )

    @property
    def aspect(self):
        return self.render_args["aspect"]

    @aspect.setter
    def aspect(self, aspect_new):
        self.render_args["aspect"] = aspect_new


class CylinderConditionalDataset(ToyDataset):
    manifold: Cylinder

    def __init__(
        self, interval_radii, transform=None, sample_tuples=True, device="cpu"
    ):
        super().__init__(
            Cylinder(interval_radii),
            transform=transform,
            sample_tuples=sample_tuples,
            sampling_strategy="conditional",
            device=device,
        )

    def sample_partner(
        self,
        x: torch.Tensor,
        eps: float = None,
        eps_rel: float = None,
        eps_min: float = None,
        eps_min_rel: float = None,
    ):
        """Sample points y uniformly in ball around x.

        The ball is taken wrt the maximum norm, so epsilon is scaled accordingly by a
        factor of sqrt(dim) to ensure dist(x,y) <= eps with the proper induced distance.

        Parameters
        ----------
        x: torch.Tensor of shape [batch_dim, self.cylinder.embed_dim]
            center points
        eps: float
            maximal distance of partner (absolute)
        eps_rel: float between 0 and 1
            maximal distance of partner relative to diameter
        eps_min: float
            minimal distance of partner (absolute)
        eps_min_rel: float between 0 and 1
            minimal distance of partner relative to diameter

        Returns
        -------
        y: array of shape [batch_dim, self.cylinder.embed_dim]
            partners to each x
        dist: array of shape [batch_dim,]
            squared distances between x and y
        interpolation: array of shape [batch_dim, ...], same shape as x
            geodesic midpoint between x and y
        """
        super().check_eps(eps, eps_rel, eps_min, eps_min_rel)
        if eps is None:
            eps = self.manifold.diameter * eps_rel
        if eps_min is None:
            eps_min = self.manifold.diameter * eps_min_rel
        # scale because of maximum norm
        eps = eps / self.manifold.dim**0.5
        eps_min = eps_min / self.manifold.dim**0.5
        # circle part
        angle_x = torch.atan2(x[:, 1], x[:, 0]).unsqueeze(-1)  # shape (batch_dim, 1)
        angle_diff = self.sample_interval(
            torch.zeros_like(angle_x), eps, eps_min, -np.pi, np.pi
        )
        angle_y = angle_x + angle_diff
        angle_av = angle_x + angle_diff / 2
        dist_angle = angle_diff**2
        # linear part
        x_linear = x[:, 2:]  # shape (batch_dim, ...)
        min_val = to_torch_and_device(
            -self.manifold.interval_radii.reshape(1, -1), self.device
        )
        max_val = to_torch_and_device(
            self.manifold.interval_radii.reshape(1, -1), self.device
        )
        y_linear = self.sample_interval(x_linear, eps, eps_min, min_val, max_val)
        av_linear = (x_linear + y_linear) / 2
        dist_linear = torch.sum((x_linear - y_linear) ** 2, dim=-1, keepdim=True)
        # to extrinsic
        y = self.manifold.intrinsic_to_extrinsic(angle_y, y_linear)
        av = self.manifold.intrinsic_to_extrinsic(angle_av, av_linear)
        return y, (dist_angle + dist_linear).squeeze(), av

    def sample_interval(self, x, eps, eps_min, min_val=None, max_val=None):
        r"""Sample values uniformly in the interval I_x

        :math:`I_x = (x-eps, x+eps)\setminus (x-eps_min, x+eps_min) \cap
        [min_val, max_val]`

        Parameters
        ----------
        x: torch.Tensor of shape [batch_size, n]
            center points
        eps: float
            maximal distance of partner (absolute)
        eps_min: float
            minimal distance of partner (absolute)
        min_val: float
            minimal value for return values
        max_val
            maximum value for return values

        Returns
        -------
        y: torch.Tensor of shape [batch_size, n]
            points in interval I_x
        """
        # I = [max(x-eps, min_val), x-eps_min] \cup [x+eps_min, min(x+eps_min, max_val)]
        #   = I_1 \cup I_2
        # sample points in both and choose which one to take according to sizes of
        # I_1 and I_2
        if min_val is not None:
            low1 = torch.maximum(x - eps, torch.ones_like(x) * min_val)
        else:
            low1 = x - eps
        high1 = x - eps_min
        low2 = x + eps_min
        if max_val is not None:
            high2 = torch.minimum(x + eps, torch.ones_like(x) * max_val)
        else:
            high2 = x + eps
        y1 = rand_tensor(x.shape, low1, high1, self.device)
        y2 = rand_tensor(x.shape, low2, high2, self.device)
        size1 = high1 - low1
        size2 = high2 - low2
        total_size = size1 + size2
        choices = (rand_tensor(x.shape, 0, 1, self.device) < size1 / total_size).float()
        y = y1 * choices + y2 * (1 - choices)
        return y
