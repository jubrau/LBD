import json
from collections import defaultdict
from typing import Union, Iterable

import numpy as np
import torch
from sklearn.decomposition import PCA
from torch.nn import MSELoss

from lbad.loss import L2, bending_distortion_loss
from lbad.models.autoencoder import Encoder
from lbad.utils.training import setup_networks


def eval_mse(dataloader, network):
    loss = 0
    loss_fun = MSELoss(reduction="sum")
    with torch.no_grad():
        for sample in dataloader:
            x_p_pred = network(sample["x"])
            loss += loss_fun(x_p_pred, sample["x_p"])
    return loss / x_p_pred.shape[-1] / dataloader.num


def eval_reconstruction(
    dataloader, autoencoder, used_images=("x", "y", "interpolations")
):
    """
    Evaluate average reconstruction loss.

    Parameters
    ----------
    dataloader: Dataloader
        dataloader for dataset returning triples (if used_images is specified) or
        tensors, if used_images is None
    autoencoder: Autoencoder
        autoencoder network
    used_images: tuple of strings or None
        names of the elements in the triple to take into account

    Returns
    -------
    loss: float
        average squared L2 loss per image channel
    """
    loss = 0
    for sample in dataloader:
        if used_images is not None:
            for image in used_images:
                x = sample[image]
                loss += L2(autoencoder(x) - x).item()
        else:
            x = sample
            loss += L2(autoencoder(x) - x).item()
    if used_images is not None:
        return loss / dataloader.num / len(used_images)
    else:
        return loss / dataloader.num


def eval_interpolation(dataloader, autoencoder, agg=True, reference="endpoints"):
    """
    Evaluate interpolation error between true interpolations and generated
    interpolations by linear interpolation in latent space.

    Use a reference value to compare to: either average reconstruction error on the
    endpoints ('endpoints'), or reconstruction error on the true interpolation
    ('interpolation').

    Parameters
    ----------
    dataloader: Dataloader
        dataloader for dataset returning triples
    autoencoder: Autoencoder
        autoencoder network
    agg: bool
        if true, return one scalar, sum over interpolation error - reference error,
        else return lists containing the returned value for each data point
    reference: string
        one of 'endpoints', 'interpolation' or None, determine how to calculate
        reference error

    Returns
    -------
    If agg:
    aggregated loss: float
        sum over interpolation error - reference error
    Elif reference is not None:
    loss, loss_ref: tuple of np.arrays
        array of squared interpolation errors, array of squared reference errors
    Else:
    loss: np.array
        array of squared interpolation errors
    """
    loss = []
    loss_ref = []
    for sample in dataloader:
        gen_interp = autoencoder.interpolate_images(sample["x"], sample["y"], 0.5)
        if reference == "endpoints":
            loss_x = (
                L2(autoencoder(sample["x"]) - sample["x"], batch_sum=False).cpu().numpy()
            )
            loss_y = (
                L2(autoencoder(sample["y"]) - sample["y"], batch_sum=False).cpu().numpy()
            )
            loss_ref_item = (loss_x + loss_y) / 2
        elif reference == "interpolation":
            loss_ref_item = (
                L2(
                    autoencoder(sample["interpolations"]) - sample["interpolations"],
                    batch_sum=False,
                )
                .cpu()
                .numpy()
            )
        if reference:
            loss_ref.append(loss_ref_item)
        loss.append(
            L2(sample["interpolations"] - gen_interp, batch_sum=False).cpu().numpy()
        )
        gen_interp = None
        loss_x = None
        loss_x = None
        loss_ref_item = None
        torch.cuda.empty_cache()
    if reference:
        loss_ref = np.concatenate(loss_ref)
    else:
        loss_ref = 0
    loss = np.concatenate(loss)
    if agg:
        error = np.sum(np.maximum(loss-loss_ref, 0))/dataloader.num
        return error
    elif reference is not None:
        return loss, loss_ref
    else:
        return loss

def eval_latent_interpolation_distance(dataloader, encoder: Encoder, agg=True):
    if agg:
        loss = 0
    else:
        loss = []
    for sample in dataloader:
        average_of_latent = encoder.interpolate_images(sample["x"], sample["y"], 0.5)
        latent_of_average = encoder(sample["interpolations"])
        if agg:
            loss += L2(
                average_of_latent - latent_of_average, batch_sum=True, mean=False
            ).item()
        else:
            loss.append(
                L2(average_of_latent - latent_of_average, batch_sum=False, mean=False)
                .cpu()
                .numpy()
            )
        average_of_latent = None
        latent_of_average = None
        torch.cuda.empty_cache()
    if agg:
        return loss / dataloader.num
    else:
        return np.concatenate(loss)


def eval_bending_distortion_loss(
    dataloader, encoder: Encoder, lambda_hess, lambda_grad=1, sum_up=True, c=0
):
    """
    Evaluate low bending and low distortion loss.

    Parameters
    ----------
    dataloader: Dataloader
        dataloader for dataset returning triples
    encoder: Encoder
        encoder network
    lambda_hess: float
        flatness weight
    lambda_grad: float
        isometry weight, default: 1.0
    sum_up: bool
        if true, sum up all loss values, else append them in a list
    c: float
        constant in isometry penalty

    Returns
    -------
    losses: dict
        dict with keys being keys of dict returned by bending_distortion_loss,
        values either list (if sum_up is false) or floats

    """
    loss_fun = bending_distortion_loss(lambda_hess, lambda_grad, sum_up, c)
    encoder.eval()
    if not sum_up:
        losses = defaultdict(lambda: [])
    else:
        losses = defaultdict(lambda: 0)
    with torch.no_grad():
        for sample in dataloader:
            x, y = sample["x"], sample["y"]
            x_code = encoder(x)
            y_code = encoder(y)
            interpolated_code = (x_code + y_code) / 2
            code_of_interpolation = encoder(sample["interpolations"])
            loss_latent = loss_fun(
                interpolated_code,
                code_of_interpolation,
                sample["distances"],
                x_code,
                y_code,
            )
            for key, value in loss_latent.items():
                if not sum_up:
                    losses[key].append(value.detach().cpu().numpy())
                else:
                    losses[key] += value.item()
            x = None
            y = None
            torch.cuda.empty_cache()
    if not sum_up:
        for key, value in losses.items():
            losses[key] = np.concatenate(value)
    else:
        for key, value in losses.items():
            losses[key] = value / dataloader.num
    return dict(losses)


def eval_pca(
    array, num_components: Union[int, list[int]] = 5
) -> Union[float, list[float]]:
    """Calculate percentage of variance explained by first num_components components.

    Parameters
    ----------
    array: np.ndarray
        numpy array of shape [num_samples, n]
    num_components: {int, list of ints}
        values for number of components to consider

    Returns
    -------
    exp_var: {float, list of float}
        percentage of variance explained by the first num_components components

    """
    pca = PCA(n_components=array.shape[-1])
    if not np.all(np.isfinite(array)):
        return (
            [np.nan] * len(num_components)
            if isinstance(num_components, Iterable)
            else np.nan
        )
    pca.fit(array)
    if isinstance(num_components, Iterable):
        exp_var = [
            float(np.sum(pca.explained_variance_ratio_[:n])) for n in num_components
        ]
    else:
        exp_var = float(np.sum(pca.explained_variance_ratio_[:num_components]))
    return exp_var


def eval_experiment(
    path,
    weights_name,
    dataloader,
    evaluation_functions,
    save=False,
    device=None,
    args_file="args.json",
):
    """
    Evaluate an experiment.

    Parameters
    ----------
    path: string
        path of experiment
    weights_name: string
        name of weights file
    dataloader: Dataloader
        dataloader used for evaluation
    evaluation_functions: dict
        dictionary of evaluation functions to run, keys are names used to return results
    save: bool
        if true, save results at path
    device: string
        on which device to run evaluation
    args_file: string, optional
        name of file containing arguments, default: args.json

    Returns
    -------
    results: dict
        dictionary corresponding to evaluation_functions dictionary, dict[key]=return
        value of evaluation_functions[key]

    """
    with open(f"{path}/{args_file}", "r") as f:
        args = json.load(f)
    include_decoder = args["architecture"].get("include_decoder", False)

    if include_decoder:
        encoder, decoder, autoencoder = setup_networks(
            args["architecture"], include_decoder=True
        )
        network = autoencoder
    else:
        encoder = setup_networks(args["architecture"], include_decoder=False)
        network = encoder

    if device is not None:
        network.to(device)

    state_dict = torch.load(f"{path}/{weights_name}")
    network.load_state_dict(state_dict)

    results = {}

    with torch.no_grad():
        for metric, function in evaluation_functions.items():
            result = function(dataloader, network)
            results[metric] = result

    if save:
        with open(f"{path}/results.json", "w") as f:
            json.dump(results, f, indent=4)
    return results
