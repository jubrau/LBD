from torch import nn as nn
from torch.nn.functional import softplus


class LeakySoftplus(nn.Module):
    r"""
    Implements the approximation of the Leaky ReLU activation with softplus, i.e.
    .. math::

        f(x)=\frac{1}{\beta } \log(\exp(\beta x) + \exp(\beta \alpha x))
            =x + \frac{1}{\beta } \log(1+\exp(\beta x(\alpha-1))
    """

    def __init__(self, alpha=None, beta=1):
        super().__init__()
        if alpha is None:
            self.negative_slope = nn.LeakyReLU().negative_slope
        else:
            self.negative_slope = alpha
        self.beta = beta

    def forward(self, x):
        return x + softplus((self.negative_slope - 1) * x, beta=self.beta)
